# Natural PHP
###The open source, PHP framework for natural development
***
Natural PHP is a framework that incorporates other open source projects
to provide a feature rich platform for app development.
Develop fast, develop Naturally !

## Requirements

* [PHP Composer] (https://getcomposer.org/)
* Apache mod_rewrite enabled
